$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("UniversalJobSearch.feature");
formatter.feature({
  "line": 2,
  "name": "Universal Job Search Example",
  "description": "",
  "id": "universal-job-search-example",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@dwpexample"
    }
  ]
});
formatter.before({
  "duration": 7558583752,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I navigate to \"https://www.gov.uk/jobsearch\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.gov.uk/jobsearch",
      "offset": 15
    }
  ],
  "location": "StepDefinitions.I_navigate_to(String)"
});
formatter.write("Step: Navigate_to_URL: Passed: Successfully launched URL: https://www.gov.uk/jobsearch\n");
formatter.result({
  "duration": 2530056071,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Job search with a valid search criteria",
  "description": "",
  "id": "universal-job-search-example;job-search-with-a-valid-search-criteria",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "I am on \"Universal Jobmatch page\"",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "I \"perform jobsearch\" on \"Universal Jobmatch page\" with values \"test analyst; Cm13\"",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "\"validresult\" is displayed on \"search results page\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Universal Jobmatch page",
      "offset": 9
    }
  ],
  "location": "StepDefinitions.I_am_on_the_page(String)"
});
formatter.result({
  "duration": 1264852,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "perform jobsearch",
      "offset": 3
    },
    {
      "val": "Universal Jobmatch page",
      "offset": 26
    },
    {
      "val": "test analyst; Cm13",
      "offset": 64
    }
  ],
  "location": "StepDefinitions.I_execute_a_page_service_with_parameters(String,String,String)"
});
formatter.write("Step: jobsearch: Passed: Successfully set the value \u0027test analyst\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (6513e3e5-76d0-478e-ab20-908fa40cdfa7)] -\u003e id: search_title]\n");
formatter.write("Step: jobsearch: Passed: Successfully set the value \u0027 Cm13\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (6513e3e5-76d0-478e-ab20-908fa40cdfa7)] -\u003e id: search_where]\n");
formatter.write("Step: jobsearch: Passed: Successfully performed \u0027Click\u0027 operation on Element: Proxy element for: DefaultElementLocator \u0027By.className: button\u0027\n");
formatter.result({
  "duration": 3617879626,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "validresult",
      "offset": 1
    },
    {
      "val": "search results page",
      "offset": 31
    }
  ],
  "location": "StepDefinitions.validate_object_is_displayed(String,String)"
});
formatter.write("Step: check_page_element: Passed: Successfully found web element: [[FirefoxDriver: firefox on WINDOWS (6513e3e5-76d0-478e-ab20-908fa40cdfa7)] -\u003e xpath: //*[@id\u003d\u0027MasterPage1_MainContent__ctlResultsRangeAndPage1_searchSummary\u0027]]\n");
formatter.result({
  "duration": 70606482,
  "status": "passed"
});
formatter.after({
  "duration": 12508445,
  "status": "passed"
});
formatter.before({
  "duration": 4343862089,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I navigate to \"https://www.gov.uk/jobsearch\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.gov.uk/jobsearch",
      "offset": 15
    }
  ],
  "location": "StepDefinitions.I_navigate_to(String)"
});
formatter.write("Step: Navigate_to_URL: Passed: Successfully launched URL: https://www.gov.uk/jobsearch\n");
formatter.result({
  "duration": 1112182360,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Job search with invalid search criteria",
  "description": "",
  "id": "universal-job-search-example;job-search-with-invalid-search-criteria",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 13,
  "name": "I am on \"Universal Jobmatch page\"",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "I \"perform jobsearch\" on \"Universal Jobmatch page\" with values \"test analyst; xyz\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "\"zeroresult\" is displayed on \"search results page\"",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "the text value displayed for \"zeroresult\" on \"search results page\" is \"We\u0027re sorry, but we couldn\u0027t find any jobs that match your criteria.\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Universal Jobmatch page",
      "offset": 9
    }
  ],
  "location": "StepDefinitions.I_am_on_the_page(String)"
});
formatter.result({
  "duration": 138862,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "perform jobsearch",
      "offset": 3
    },
    {
      "val": "Universal Jobmatch page",
      "offset": 26
    },
    {
      "val": "test analyst; xyz",
      "offset": 64
    }
  ],
  "location": "StepDefinitions.I_execute_a_page_service_with_parameters(String,String,String)"
});
formatter.write("Step: jobsearch: Passed: Successfully set the value \u0027test analyst\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (46b46b75-cb16-4c35-ba69-ac2e39e13c88)] -\u003e id: search_title]\n");
formatter.write("Step: jobsearch: Passed: Successfully set the value \u0027 xyz\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (46b46b75-cb16-4c35-ba69-ac2e39e13c88)] -\u003e id: search_where]\n");
formatter.write("Step: jobsearch: Passed: Successfully performed \u0027Click\u0027 operation on Element: Proxy element for: DefaultElementLocator \u0027By.className: button\u0027\n");
formatter.result({
  "duration": 2161491039,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "zeroresult",
      "offset": 1
    },
    {
      "val": "search results page",
      "offset": 30
    }
  ],
  "location": "StepDefinitions.validate_object_is_displayed(String,String)"
});
formatter.write("Step: check_page_element: Passed: Successfully found web element: [[FirefoxDriver: firefox on WINDOWS (46b46b75-cb16-4c35-ba69-ac2e39e13c88)] -\u003e xpath: //*[@id\u003d\u0027MasterPage1_MainContent__ctlZeroResults_MONSMessage1\u0027]]\n");
formatter.result({
  "duration": 209047038,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "zeroresult",
      "offset": 30
    },
    {
      "val": "search results page",
      "offset": 46
    },
    {
      "val": "We\u0027re sorry, but we couldn\u0027t find any jobs that match your criteria.",
      "offset": 71
    }
  ],
  "location": "StepDefinitions.the_text_value_displayed_in_is(String,String,String)"
});
formatter.write("Step: check text value of Weblement: Passed: Successfully checked text value for web element: [[FirefoxDriver: firefox on WINDOWS (46b46b75-cb16-4c35-ba69-ac2e39e13c88)] -\u003e xpath: //*[@id\u003d\u0027MasterPage1_MainContent__ctlZeroResults_MONSMessage1\u0027]] as:We\u0027re sorry, but we couldn\u0027t find any jobs that match your criteria.\n");
formatter.result({
  "duration": 509029349,
  "status": "passed"
});
formatter.after({
  "duration": 9050782,
  "status": "passed"
});
formatter.before({
  "duration": 5344823256,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I navigate to \"https://www.gov.uk/jobsearch\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.gov.uk/jobsearch",
      "offset": 15
    }
  ],
  "location": "StepDefinitions.I_navigate_to(String)"
});
formatter.write("Step: Navigate_to_URL: Passed: Successfully launched URL: https://www.gov.uk/jobsearch\n");
formatter.result({
  "duration": 783480376,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Job search with valid search criteria from datasheet",
  "description": "",
  "id": "universal-job-search-example;job-search-with-valid-search-criteria-from-datasheet",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 19,
  "name": "I am on \"Universal Jobmatch page\"",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "I \"perform jobsearch\" on \"Universal Jobmatch page\" with values \"datasheet_jobtitle; datasheet_postcode\"",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "\"validresult\" is displayed on \"search results page\"",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "\"zeroresult\" is not displayed on \"search results page\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Universal Jobmatch page",
      "offset": 9
    }
  ],
  "location": "StepDefinitions.I_am_on_the_page(String)"
});
formatter.result({
  "duration": 148522,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "perform jobsearch",
      "offset": 3
    },
    {
      "val": "Universal Jobmatch page",
      "offset": 26
    },
    {
      "val": "datasheet_jobtitle; datasheet_postcode",
      "offset": 64
    }
  ],
  "location": "StepDefinitions.I_execute_a_page_service_with_parameters(String,String,String)"
});
formatter.write("Step: Get_DataSheetValue: Passed: Successfully found data for column:[JOBTITLE] as:\u0027test analyst\u0027\n");
formatter.write("Step: jobsearch: Passed: Successfully set the value \u0027test analyst\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (2d40c95e-4479-4f24-8e8c-acd87aaca8b6)] -\u003e id: search_title]\n");
formatter.write("Step: Get_DataSheetValue: Passed: Successfully found data for column:[POSTCODE] as:\u0027CM13\u0027\n");
formatter.write("Step: jobsearch: Passed: Successfully set the value \u0027CM13\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (2d40c95e-4479-4f24-8e8c-acd87aaca8b6)] -\u003e id: search_where]\n");
formatter.write("Step: jobsearch: Passed: Successfully performed \u0027Click\u0027 operation on Element: Proxy element for: DefaultElementLocator \u0027By.className: button\u0027\n");
formatter.result({
  "duration": 4498105742,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "validresult",
      "offset": 1
    },
    {
      "val": "search results page",
      "offset": 31
    }
  ],
  "location": "StepDefinitions.validate_object_is_displayed(String,String)"
});
formatter.write("Step: check_page_element: Passed: Successfully found web element: [[FirefoxDriver: firefox on WINDOWS (2d40c95e-4479-4f24-8e8c-acd87aaca8b6)] -\u003e xpath: //*[@id\u003d\u0027MasterPage1_MainContent__ctlResultsRangeAndPage1_searchSummary\u0027]]\n");
formatter.result({
  "duration": 212418969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "zeroresult",
      "offset": 1
    },
    {
      "val": "search results page",
      "offset": 34
    }
  ],
  "location": "StepDefinitions.validate_object_is_not_displayed(String,String)"
});
formatter.write("Step: check_page_element_notdisplayed: Passed: Successfully checked non existence of web element: Proxy element for: DefaultElementLocator \u0027By.xpath: //*[@id\u003d\u0027MasterPage1_MainContent__ctlZeroResults_MONSMessage1\u0027]\u0027\n");
formatter.result({
  "duration": 135548605,
  "status": "passed"
});
formatter.after({
  "duration": 248517045,
  "status": "passed"
});
formatter.before({
  "duration": 4898292662,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I navigate to \"https://www.gov.uk/jobsearch\"",
  "keyword": "Given "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.gov.uk/jobsearch",
      "offset": 15
    }
  ],
  "location": "StepDefinitions.I_navigate_to(String)"
});
formatter.write("Step: Navigate_to_URL: Passed: Successfully launched URL: https://www.gov.uk/jobsearch\n");
formatter.result({
  "duration": 872292267,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "User can submit a comment via the general contact form",
  "description": "",
  "id": "universal-job-search-example;user-can-submit-a-comment-via-the-general-contact-form",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 25,
  "name": "I am on \"Universal Jobmatch page\"",
  "keyword": "Given "
});
formatter.step({
  "line": 26,
  "name": "I \"invoke contact form\" on \"Universal Jobmatch page\"",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "I \"fill up the form\" on \"Contact Us page\"",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I \"submit the form\" on \"Contact Us page\"",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "\"your message has been sent\" is displayed on \"Contact Us page\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Universal Jobmatch page",
      "offset": 9
    }
  ],
  "location": "StepDefinitions.I_am_on_the_page(String)"
});
formatter.result({
  "duration": 137051,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "invoke contact form",
      "offset": 3
    },
    {
      "val": "Universal Jobmatch page",
      "offset": 28
    }
  ],
  "location": "StepDefinitions.I_execute_a_page_service(String,String)"
});
formatter.write("Step: Invoke contact form: Passed: Successfully performed \u0027Click\u0027 operation on Element: Proxy element for: DefaultElementLocator \u0027By.xpath: //*[@id\u003d\u0027before-you-start\u0027]/p[6]/a\u0027\n");
formatter.result({
  "duration": 1339091879,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "fill up the form",
      "offset": 3
    },
    {
      "val": "Contact Us page",
      "offset": 25
    }
  ],
  "location": "StepDefinitions.I_execute_a_page_service(String,String)"
});
formatter.write("Step: Contact us-Form Fill: Passed: Successfully set the value \u0027test\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_FirstName]\n");
formatter.write("Step: Contact us-Form Fill: Passed: Successfully set the value \u0027user\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_LastName]\n");
formatter.write("Step: Contact us-Form Fill: Passed: Successfully set the value \u0027SW1H 9NA\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_ZIPCode]\n");
formatter.write("Step: Contact us-Form Fill: Passed: Successfully set the value \u0027test@yahoo.com\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_Email]\n");
formatter.write("Step: Contact us-Form Fill: Passed: Successfully performed \u0027Click\u0027 operation on Element: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_ExistingUser_0]\n");
formatter.write("Step: Contact us-Form Fill: Passed: Successfully performed \u0027SelectListItem\u0027 operation on Element: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_ddlSubject]\n");
formatter.write("Step: Contact us-Form Fill: Passed: Successfully set the value \u0027test comment\u0027 in Edit box: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: MasterPage1_MainContent_feedback_Question]\n");
formatter.result({
  "duration": 1760107491,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "submit the form",
      "offset": 3
    },
    {
      "val": "Contact Us page",
      "offset": 24
    }
  ],
  "location": "StepDefinitions.I_execute_a_page_service(String,String)"
});
formatter.write("Step: Submit contact form: Passed: Successfully performed \u0027Click\u0027 operation on Element: Proxy element for: DefaultElementLocator \u0027By.id: MasterPage1_MainContent_feedback_btnSend\u0027\n");
formatter.result({
  "duration": 687293707,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "your message has been sent",
      "offset": 1
    },
    {
      "val": "Contact Us page",
      "offset": 46
    }
  ],
  "location": "StepDefinitions.validate_object_is_displayed(String,String)"
});
formatter.write("Step: Check Object Existence: Passed: Successfully found web element: [[FirefoxDriver: firefox on WINDOWS (b20d5761-9806-4c56-a8d2-e83b39d89c0c)] -\u003e id: confirmationMessage]\n");
formatter.result({
  "duration": 69818592,
  "status": "passed"
});
formatter.after({
  "duration": 45985048,
  "status": "passed"
});
});