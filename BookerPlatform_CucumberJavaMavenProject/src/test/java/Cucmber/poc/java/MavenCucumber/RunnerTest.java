package Cucmber.poc.java.MavenCucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
	plugin = {"pretty", "html:target/cucumber-html-report", "json:target/cucumber-json-report.json"},
	features = "src/test/resource/",
	//glue = "Cucumber.poc.java.MavenCucumber"
	tags = {"@dwpexample"}
		
)

public class RunnerTest {

}
