package Cucmber.poc.java.MavenCucumber;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import cucumber.api.Scenario;

public class HotelPlatform_page extends CoreFunctions{
	
	//Declarations
	WebDriver driver;
	Scenario scenario;
	
	//Page Element Locators
	@FindBy(xpath="/html/body/div/nav/div[1]/ul/li[2]/a")
	WebElement login;
	
	@FindBy(id="username")
	WebElement username;
	
	@FindBy(id="password")
	WebElement password;
	
	@FindBy(id="doLogin")
	WebElement login_btn;
	
	@FindBy(xpath="/html/body/div/nav/div[1]/ul/li[1]/a")
	WebElement home;
	
	@FindBy(id="hotelName")
	WebElement hotelname;
	
	@FindBy(id="address")
	WebElement address;
	
	@FindBy(id="owner")
	WebElement owner;
	
	@FindBy(id="phone")
	WebElement phone;
	
	@FindBy(id="email")
	WebElement email;
	
	@FindBy(id="createHotel")
	WebElement create;
	
	@FindBy(xpath="/html/body/div/div[2]")
	WebElement one_entry_row;	
	
	@FindBy(xpath="/html/body/div/div[3]")
	WebElement second_entry_row;	
	
	@FindBy(xpath ="//input[@type='hidden']")
	WebElement delete_row_one;
	
	
	//Class constructor
	public HotelPlatform_page(WebDriver driver,Scenario scenario){
		this.driver=driver;
		this.scenario = scenario;
		PageFactory.initElements(driver,this);
	}
	
	//return page webelement
	public WebElement return_page_element(String ElmNm){
		WebElement MyElm = null;
		ElmNm = ElmNm.toLowerCase();		
		if (ElmNm.contentEquals("one entry row")){
			MyElm = one_entry_row;
		}else if (ElmNm.contentEquals("second row")) {
			MyElm = second_entry_row;
		}else if(ElmNm.contentEquals("delete row one")) {
			MyElm = delete_row_one;
		} 
		return MyElm;
	}
	
	//Page Service/Method Distributor
	public void Execute_Process(String Process_Name, String Param_List) {
		String [] args = null;
		args = Param_List.split(";");
		
		if (Process_Name.toUpperCase().contains("CREATE AN ENTRY")) {
			create_entry();			
		} else if(Process_Name.toUpperCase().contains("DELETE ROW ONE")) {
			delete_entry();
		} else if (Process_Name.toUpperCase().equals("CHECK_PAGE_ELEMENT_DISPLAYED")) {
			check_page_element_displayed(args[0]);
		}else if (Process_Name.toUpperCase().equals("CHECK_PAGE_ELEMENT_NOTDISPLAYED")) {
			check_page_element_notdisplayed(args[0]);
		}else if (Process_Name.toUpperCase().equals("LOG INTO PLATFORM")) {
			login();
		}
	}
	
	//Services/Methods offered by the page
	public void login(){
		String Step_ID = "login to platform";
		ClickElement(scenario,Step_ID,login);
		String handle= driver.getWindowHandle(); //Get login window handle
		driver.switchTo().window(handle);       
		SetEditBox(scenario,Step_ID,username,"admin");
		SetEditBox(scenario,Step_ID,password,"password");
		ClickElement(scenario,Step_ID,login_btn);
		Page_Sync(driver);
		//String handle1= driver.getWindowHandle(); //Get platform window handle
		//driver.switchTo().window(handle1); 
	}
	public void create_entry () {
		String Step_ID = "create an entry";
		Wait_untillelementvisible(driver, hotelname);
		SetEditBox(scenario,Step_ID,hotelname,"testhotel");
		SetEditBox(scenario,Step_ID,address,"testaddress");
		SetEditBox(scenario,Step_ID, owner, "testowner");
		SetEditBox(scenario,Step_ID, phone, "032435235");
		SetEditBox(scenario,Step_ID, email, "test@yahoo.com");
		ClickElement(scenario,Step_ID,create);
		Page_Sync(driver);
		//Wait_untillelementvisible(driver, one_entry_row);
	}
	
	public void delete_entry(){
		String Step_ID = "delete an entry";
		String myID = delete_row_one.getAttribute("id");
		WebElement myElement = driver.findElement(By.id(myID));
		ClickElement(scenario,Step_ID, myElement);
		Page_Sync(driver);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
	
	public void check_page_element_displayed(String element){
		Wait_untillelementvisible(driver, return_page_element(element));
		CheckObjectExistence(scenario,"check_page_element",return_page_element(element));		
	}
	
	public void check_page_element_notdisplayed(String element){
		CheckObjectNonExistence(scenario,"check_page_element_notdisplayed",return_page_element(element));
	}
}
