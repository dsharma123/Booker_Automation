package Cucmber.poc.java.MavenCucumber;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class StepDefinitions extends CoreFunctions {
	
	String stp_nm;
	public Scenario scenario;
	public WebDriver driver;
	public String process_results,scenario_name;
	
	//Start up routine	
	@Before 
	public void I_start_test_execution(Scenario scenario) throws Throwable {
		this.scenario = scenario;
		driver = TestStartUp(scenario,"Start_test_execution");	
		
	}
	
	//Tear down routine
	@After
	public void i_end_test_execution(Scenario scenario) throws Throwable {
		driver.findElement(By.id("logout")).click();
		BrowserClose(scenario,"Stop_test_execution");
		if (scenario.isFailed()) {
		      try {
		        byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		        scenario.embed(screenshot, "image/png");
		      } catch (WebDriverException wde) {
		        System.err.println(wde.getMessage());
		      } catch (ClassCastException cce) {
		        cce.printStackTrace();
		      }
		    }
	}
	
	//Navigate to a web URL
	@Given("^I navigate to \"([^\"]*)\"$")
	public void I_navigate_to(String URL) throws Throwable {
		LaunchURL(scenario,"Navigate_to_URL" , URL);
		Page_Sync(driver);
	}
	
	
	@Given("^I am on \"([^\"]*)\"$")
	public void I_am_on_the_page(String page_name) throws Throwable {
	    System.out.println("page checked");
	}
	
	//Execute a page service with parameters
	@When("^I \"([^\"]*)\" on \"([^\"]*)\" with values \"([^\"]*)\"$")
	public void I_execute_a_page_service_with_parameters(String Process_Name, String Page_Name, String Param_List) throws Throwable {
		Execute_Page_Function(Process_Name, Page_Name, Param_List);
		
	}
	
	//Execute a page service without parameters
	@When("^I \"([^\"]*)\" on \"([^\"]*)\"$")
	public void I_execute_a_page_service(String Process_Name, String Page_Name) throws Throwable {
		Execute_Page_Function(Process_Name, Page_Name, "Empty");
		
	}

	//Validate object existence on a page
	@Then("^\"([^\"]*)\" is displayed on \"([^\"]*)\"$")
	public void validate_object_is_displayed(String Object_Name, String Page_Name) throws Throwable {
		Execute_Page_Function("check_page_element_displayed", Page_Name, Object_Name);
	}
	
	//Validate object non existence on a page
	@Then("^\"([^\"]*)\" is not displayed on \"([^\"]*)\"$")
	public void validate_object_is_not_displayed(String Object_Name, String Page_Name) throws Throwable {
		Execute_Page_Function("check_page_element_notdisplayed", Page_Name, Object_Name);
	}
	
	//Validate text value for an object on a page
	@Then("^the text value displayed for \"([^\"]*)\" on \"([^\"]*)\" is \"([^\"]*)\"$")
	public void the_text_value_displayed_in_is(String Object_Name, String Page_Name, String txtval) throws Throwable {
		Execute_Page_Function("check_page_element_text", Page_Name, Object_Name + ";" + txtval);
	}
	
	//Instantiate page object and execute page service/method
	public void Execute_Page_Function(String Process_Name, String Page_Name, String Param_List) {	
		//process_results = "";
		if (Page_Name.toUpperCase().contentEquals("HOTEL MANAGEMENT PLATFORM PAGE")) {	
				HotelPlatform_page jobsearch = new HotelPlatform_page(driver,scenario);
				jobsearch.Execute_Process(Process_Name,Param_List);
				jobsearch = null;
		
	}
		
	}
	
}


