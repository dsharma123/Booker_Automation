package Cucmber.poc.java.MavenCucumber;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import org.junit.Assert;
import org.junit.internal.runners.statements.Fail;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebDriverException;
import cucumber.api.Scenario;

public class CoreFunctions{

	public static BufferedWriter out;
	public FileInputStream fs = null;
	public WebDriver driver;
	public Scenario core_scenario;
	public boolean process_flag, fail_flag;
	public String results,scenario_name;
	public byte[] screenshot;
	public static Properties RunConfig = null;	
	public static String dtsheetloc = null;	

	/*####################################################################################
	Function Name: CheckObjectExistence
	Function Description: Checks the existence of the specified object in the application
	Input Parameters: scenario, Test Step Id, WebElement
	Return Values: None
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void CheckObjectExistence(Scenario scenario,String Test_Step_Id,WebElement element)
	
	{	
		if(ValidateObject(scenario,"exists",element)){
			// Report Checkpoint Passed
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully found web element: " +element);
		} else {
			//Report Checkpoint failed
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Object exists but is not visible for web element: " +element);
		}
		
	 } 
	
	/*####################################################################################
	Function Name: CheckObjectNonExistence
	Function Description: Checks the existence of the specified object in the application
	Input Parameters: scenario, Test Step Id, WebElement
	Return Values: None
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void CheckObjectNonExistence(Scenario scenario,String Test_Step_Id,WebElement element)
	
	{	
		if(ValidateObject(scenario,"nonexistence",element)){
			// Report Checkpoint Passed
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully checked non existence of web element: " +element);
		} else {
			//Report Checkpoint failed
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to check non existence of web element: " +element);
		}
		
	 } 
	
	/*####################################################################################
	Function Name: CheckText
	Function Description: Checks text value of the specified object in the application
	Input Parameters: scenario, Test Step Id, WebElement, textval
	Return Values: None
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void CheckText(Scenario scenario, String Test_Step_Id,WebElement MyElement, String textval) 
	{
		//Exception handling- Validate that the object is visible and is enabled
		ValidateObject (scenario,"visible", MyElement);
		ValidateObject (scenario,"enabled", MyElement);
		
		String ActVal = null;
		
		try{
			ActVal = MyElement.getText();
		} catch (Exception e) {
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to perform gettext on webelement:"+MyElement+".Error occured:"+e.getStackTrace());
		}
		
		
		if(textval.contentEquals(ActVal)){
			// Report Checkpoint Passed
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully checked text value for web element: " +MyElement+ " as:"+textval);
		} else {
			// Report Checkpoint Failed
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to match text value for web element: " +MyElement+ " as:'"+textval+"'.Actual value displayed is:'"+ActVal+"'");
			
		}
		
	}
	 /*####################################################################################
		Function Name: ReportStepResult
		Function Description: Report success or failure  
		Input Parameters: scenario, stepname, status, failuremessage
		Return Values: none
		Author: Deepak
		Example:  
	####################################################################################*/
	
	public void ReportStepResult(Scenario scenario,String string, String string2, String string3)	
	
	{	
		scenario.write("Step: " + string + ": " +string2 + ": " +string3 + "\n");
		
		if (string2.contentEquals("Failed")) {
			fail_flag = true;
			Assert.fail(string3);
		}	
	}
	
	
	/*####################################################################################
	Function Name: LaunchURL
	Function Description: Launch the specified URL
	Input Parameters: scenarion, Test_Step_Id, URL
	Return Values: None
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void LaunchURL (Scenario scenario,String Test_Step_Id, String URL)
	
	{
		try{
			driver.get("" +URL);
			//Report success
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully launched URL: " +URL);
		} catch (Exception e) {
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to launch URL.Exception occured:"+e.getStackTrace());
		}
	
	}
	
	
	/*####################################################################################
	Function Name: BrowserClose
	Function Description: close the current browser session
	Input Parameters: scenario, Test_step_id
	Return Values: None
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void BrowserClose(Scenario scenario,String Test_Step_Id)
	
	{
		try {
			driver.close();
			//ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully closed the Browser session");
		} catch (Exception e) {
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to close browser session.Exception occured:"+e.getStackTrace());
		}		
	
	}
	
	/*####################################################################################
	Function Name: TestStartUp
	Function Description: Start up routine that does the necessary test set up tasks
	Input Parameters: scenario, test_step_id
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public WebDriver TestStartUp (Scenario scenario,String Test_Step_Id) throws MalformedURLException
	
	{
			
		ReadRunConfig(scenario);
		
		/*DesiredCapabilities cap = DesiredCapabilities.firefox();
		
		cap.setPlatform(Platform.WIN10);
		
		URL url = new URL("http://localhost:4444/wd/hub");
	
		
		driver = new RemoteWebDriver(url, cap);*/
		driver = getWebdriverInstance();		
		
		fail_flag = false;
		
		//ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully created instance of Firefox driver");
		
		return driver;	
		
	}
		
	
	/*####################################################################################
	Function Name: GetExcelData
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public static String[][] GetExcelData() throws IOException 
	
	{
		dtsheetloc = RunConfig.getProperty("datasheetlocation");
		File exl = new File(dtsheetloc);
		FileInputStream fis = new FileInputStream(exl);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet ws = wb.getSheet("Sheet1");
		int rowcnt = ws.getLastRowNum()+1;
		int colcnt = ws.getRow(0).getLastCellNum();
		String [][] ExcelData = new String[rowcnt][colcnt];
		
		for (int i=0; i<rowcnt; i++) {
			XSSFRow row = ws.getRow(i);
			
			for (int j=0; j<colcnt; j++) {
				XSSFCell cell = row.getCell(j);
				String value = celltoString(cell);	
				ExcelData[i][j] = value;
			}
		}
		return ExcelData;
	}	
		
	/*####################################################################################
	Function Name: celltoString
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public static String celltoString(XSSFCell cell) 
	{
		int type;
		Object result;

		try 
		{
			type = cell.getCellType();
		} catch (NullPointerException e) {
			type = 3;
		}
	
		switch (type) {
	
		case 0 : //numeric type value
			result = cell.getNumericCellValue();
			break;
	
		case 1 : //String type value
			result = cell.getStringCellValue();
			break;
	
		default :
	
		throw new RuntimeException("There is no test data for the specified test name");
	
		}
	
		return result.toString();
	
	}
	
	
	
	/*####################################################################################
	Function Name: GetScenarioData_ByColumn
	Function Description: Get the test data from data sheet for the specified column and row
	Input Parameters: Step Id, Column Name, Row Number
	Return Values: Data value
	Author: Deepak
	Example:  
	####################################################################################*/

	public String GetScenarioData_ByColumn(Scenario scenario,String Test_Step_Id,String ColumnName, String ScenarioName) throws IOException
	{
		ColumnName = ColumnName.replace("datasheet_", "");
		ColumnName = ColumnName.trim();
		String[][] data;
		data = GetExcelData();
		String ColNm,RowNm;
		Integer RowID = 0;
		String ColVal = null;
		
		//find scenario row number
		for (int j = 0; j<data.length; j++){
			RowNm = data[j][0];
			System.out.println(RowNm);
		
			if(ScenarioName.contentEquals(RowNm)){	//found ColumnName
				RowID = j;
				System.out.println(RowID);
				break;
			}		
		}
			
		if(RowID == 0){
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Unable to find the data row for specified scenario:[" +ScenarioName+ "] in test data excel sheet");
		}
			
		for (int i = 0; i<data.length+1; i++){
			ColNm = data[0][i];
			
			if(ColumnName.contentEquals(ColNm)){	//found ColumnName
				ColVal = data[RowID][i];
				break;
			}	
		
		}
		
		if(ColVal == null){
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Unable to find the specified column:[" +ColumnName+ "] in test data excel sheet");
		}
		else {
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully found data for column:[" +ColumnName+ "] as:'" +ColVal+"'");
			}
		return ColVal;
		
	}
			
			
	public void Page_Sync (WebDriver driver){
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	}
		
			
	/*####################################################################################
	Function Name: SetEditBox
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void SetEditBox(Scenario scenario,String Test_Step_Id, WebElement MyElement, String Value)
	
	{
		//Exception handling- Validate that the object is visible and is enabled
		ValidateObject (scenario,"visible", MyElement);
		ValidateObject (scenario,"enabled", MyElement);
		
		//Get test data from Datasheet if applicable
		if (Value.toUpperCase().contains("DATASHEET")) {
			Value = GetTestData_ByColumnName(scenario,"Get Test Data",Value);
		}
		
		//Error Handling
		try{
			//Perform set operation
			MyElement.sendKeys(Value);
		
			//Report success
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully set the value '" +Value+ "' in Edit box: "+MyElement);
		
		} catch (Exception e){
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to set the value '" +Value+ "' in Edit box: "+MyElement+ ". Error occured:"+e.getMessage());
		}
		
	}

	/*####################################################################################
	Function Name: ClickElement
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void ClickElement(Scenario scenario,String Test_Step_Id, WebElement MyElement)
	
	{
		//Exception handling- Validate that the object is visible and is enabled
		ValidateObject (scenario,"visible", MyElement);
		ValidateObject (scenario,"enabled", MyElement);
			
		//Error Handling
		try{
			//Perform click operation
			MyElement.click();
			
			//Report success
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully performed 'Click' operation on Element: " +MyElement);
		
		} catch (Exception e){
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to perform 'Click' operation on Element: "+MyElement+ ". Error occured:"+e.getMessage());
		}
			
	}
	
	/*####################################################################################
	Function Name: SelectRadioButton
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/

	public void SelectRadioButton(Scenario scenario,String Test_Step_Id, WebElement MyElement){
		//Click the specified radio button option webelement object to select the radio button
		ClickElement(scenario, Test_Step_Id, MyElement);
		
		//Exception handling- Validate that the specified radio button has been selected
		ValidateObject (scenario,"selected", MyElement);
	}
	
	/*####################################################################################
	Function Name: SetCheckBox
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/

	public void SetCheckBox(Scenario scenario,String Test_Step_Id, WebElement MyElement){
		//Click the specified check box option webelement object to select the check box
		ClickElement(scenario, Test_Step_Id, MyElement);
		
		//Exception handling- Validate that the specified check box option has been selected
		ValidateObject (scenario,"selected", MyElement);
	}
	
	/*####################################################################################
	Function Name: SelectListItem
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public void SelectListItem(Scenario scenario,String Test_Step_Id, WebElement ListName,String ListValue)
	
	{
		//Exception handling- Validate that the object is visible and is enabled
		ValidateObject (scenario,"visible", ListName);
		ValidateObject (scenario,"enabled", ListName);
		
		//Define List object
		Select subjectlst = new Select(ListName);
		
		//Get test data from Datasheet if applicable
		if (ListValue.toUpperCase().contains("DATASHEET")) {			
			ListValue = GetTestData_ByColumnName(scenario,"Get Test Data",ListValue );
		}
		
		//Error Handling
		try{
			//Perform select operation
			subjectlst.selectByVisibleText(ListValue);
		
			ReportStepResult(scenario,Test_Step_Id, "Passed", "Successfully performed 'SelectListItem' operation on Element: " +ListName);
		
		} catch (Exception e){
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to perform 'SelectListItem' operation on Element: " +ListName+ ". Error occured:"+e.getMessage());
		}
		
	}
	
	/*####################################################################################
	Function Name: ValidateObject
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	
	public boolean ValidateObject(Scenario scenario,String Validation_Case, WebElement MyElement)
	{
		
		if (Validation_Case.toUpperCase().equals("NONEXISTENCE")) {
			try	{
				if(MyElement.isDisplayed()) {
					return false;
				}else {	//If element exists but is not visible
					return true;
				}
				
			} catch (NoSuchElementException e){
				//no such element caught;
				return true;
			}		
		} else if (Validation_Case.toUpperCase().equals("EXISTS")) {
			try	{
					if(MyElement.isDisplayed()) {
						return true;
					}else {	//If element exists but is not visible
						return false;
					}
					
				} catch (NoSuchElementException e){
					//System.out.println("no such element caught");
					ReportStepResult(scenario,"Validate_Existence", "Failed", "Failed to find Webelement : "+MyElement + ",Exiting test...");
					return false;
				}	
			
		} else if (Validation_Case.toUpperCase().equals("ENABLED")) {
			try {
					if(MyElement.isEnabled()) {
						return true;
					} else {	//If element exists but is not enabled
						return true;
					}
					
				} catch (NoSuchElementException e) {
					ReportStepResult(scenario,"Validate_Enabled", "Failed", "Failed to find Webelement : "+MyElement + ",Exiting test...");
					return false;
			}
			
		} else if (Validation_Case.toUpperCase().equals("SELECTED")) {
			boolean bValue = false;
			bValue = MyElement.isSelected();
			if (bValue != true){
				ReportStepResult(scenario,"Validate_Selected", "Failed", "Object not selected after performing select radiobutton/checkbox operation for Webelement : "+MyElement + ",Exiting test...");
			}
			return true;
		
		}  else {
			return true;
		}
	 }
	
	/*####################################################################################
	Function Name: GetTestData_ByColumnName
	Function Description: 
	Input Parameters: 
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	public String GetTestData_ByColumnName(Scenario scenario, String Test_Step_Id, String ColumnName)
	{
		String ColumnData = null;
		try {
			ColumnName = ColumnName.toUpperCase().replace("DATASHEET_", "");
			ColumnData = GetScenarioData_ByColumn(scenario,"Get_DataSheetValue", ColumnName, scenario.getName());
		} catch (IOException e) {
			//Report failure
			ReportStepResult(scenario,Test_Step_Id, "Failed", "Failed to read datasheet for test data.IO Exception occured:"+e.getStackTrace());
		}
		return ColumnData;
	}
	
	 public void takescreenshot (WebDriver driver) {
		try {	
				screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
	
			} catch (WebDriverException e){
	
				e.printStackTrace();
			}
	 }
	 
	 public static void msgbox(String s){
		   JOptionPane.showMessageDialog(null, s);
		}	
	 
	/*####################################################################################
	Function Name: ReadRunConfig
	Function Description: 
	Input Parameters: Scenario
	Return Values: 
	Author: Deepak
	Example:  
	####################################################################################*/
	 public void ReadRunConfig(Scenario scenario)
	 {
		 try{
			 	RunConfig = new Properties();
				FileInputStream fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\resource\\RunConfig.properties");
				RunConfig.load(fs);
			}catch(Exception e){
				ReportStepResult(scenario,"Read Run Configuration Properties","Failed", "Unable to read Run Config properties file");
			}
			
		}
	  
	/*####################################################################################
	Function Name: getWebdriverInstance
	Function Description: return Webdriver instance for the specified browser type
	Input Parameters: None
	Return Values: Webdriver instance
	Author: Deepak
	Example:  
	####################################################################################*/
	 public WebDriver getWebdriverInstance() 
	 {      
		 String browser_name = RunConfig.getProperty("browser");
         switch (browser_name) {
            case "firefox":
                driver = new FirefoxDriver();
                break;
            case "chrome":
            	System.setProperty("webdriver.chrome.driver", "C:/Users/User/Downloads/chromedriver_win32/chromedriver.exe");
                driver = new ChromeDriver();
                break;
            case "ie":
            	System.setProperty("webdriver.ie.driver","C:/Users/User/Downloads/IEDriverServer_Win32_2.53.1/IEDriverServer.exe");
            	driver = new InternetExplorerDriver();
                break;
            default:
                driver = new FirefoxDriver();
                break;
	        }
	        // maximize browser's window on start
	        driver.manage().window().maximize();
	        return driver;
	   }
	 
		/*####################################################################################
		Function Name: Wait_untillelementvisible
		Function Description: explicit wait till specified WebElement is visible
		Input Parameters: None
		Return Values: 
		Author: Deepak
		Example:  
		####################################################################################*/
	 public void Wait_untillelementvisible(WebDriver driver, WebElement Element){
		 WebElement myDynamicElement = (new WebDriverWait(driver, 10))
				  .until(ExpectedConditions.visibilityOf(Element));
	 }
}