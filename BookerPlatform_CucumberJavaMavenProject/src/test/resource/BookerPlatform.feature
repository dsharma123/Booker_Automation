@BookerPlatform
Feature: Creation and Deletion of Entry

  Background: 
    Given I navigate to "localhost:3003/"

  Scenario: Create an Entry
    Given I am on "Hotel Management Platform page"
    When I "log into platform" on "Hotel Management Platform page"
    And I "create an entry" on "Hotel Management Platform page"
    Then "second row" is displayed on "Hotel Management Platform page"

  Scenario: Delete an Entry
    Given I am on "Hotel Management Platform page"
    When I "log into platform" on "Hotel Management Platform page"
    And "one entry row" is displayed on "Hotel Management Platform page"
    And I "delete row one" on "Hotel Management Platform page"
    Then "second row" is not displayed on "Hotel Management Platform page"
      
  Scenario: Creation of multiple Entries
    Given I am on "Hotel Management Platform page"
    When I "log into platform" on "Hotel Management Platform page"
    And I "create an entry" on "Hotel Management Platform page"
    And I "create an entry" on "Hotel Management Platform page"
    Then "one entry row" is displayed on "Hotel Management Platform page"
    And "second row" is displayed on "Hotel Management Platform page"

